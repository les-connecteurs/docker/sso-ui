package config

import (
	"os"
	"testing"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/stretchr/testify/assert"
)

func TestClients(t *testing.T) {
	var cfg Config

	cfg.Hydra.URL = "https://hydra:1234/base"
	cfg.Kratos.URL = "https://kratos:5678/root"

	err := cfg.loadClients()
	assert.Nil(t, err)

	assert.NotNil(t, cfg.Hydra.Client)
	assert.NotNil(t, cfg.Kratos.Client)

	hydraRuntime, ok := cfg.Hydra.Client.Transport.(*httptransport.Runtime)
	assert.True(t, ok)
	assert.Equal(t, hydraRuntime.Host, "hydra:1234")
	assert.Equal(t, hydraRuntime.BasePath, "/base")

	kratosRuntime, ok := cfg.Kratos.Client.Transport.(*httptransport.Runtime)
	assert.True(t, ok)
	assert.Equal(t, kratosRuntime.Host, "kratos:5678")
	assert.Equal(t, kratosRuntime.BasePath, "/root")
}

func TestLoadFromEnvironement(t *testing.T) {
	var cfg *Config
	os.Setenv("HTTP_HOST", "localhost")
	os.Setenv("HTTP_PORT", "3000")

	cfg = Load()
	assert.Equal(t, cfg.HTTP.Host, "localhost")
	assert.Equal(t, cfg.HTTP.Port, 3000)

	os.Setenv("HTTP_HOST", "sso-ui")
	os.Setenv("HTTP_PORT", "1234")

	cfg = Load()
	assert.Equal(t, cfg.HTTP.Host, "sso-ui")
	assert.Equal(t, cfg.HTTP.Port, 1234)
}
