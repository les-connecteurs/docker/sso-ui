package config

import (
	"log"
	"net/url"
	"strings"

	"github.com/lestrrat-go/jwx/jwk"
	hydraclient "github.com/ory/hydra-client-go/client"
	kratosclient "github.com/ory/kratos-client-go/client"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"

	"connecteu.rs/sso-ui/sentry"
)

type Config struct {
	HTTP struct {
		Port int
		Host string
	}
	Kratos struct {
		URL    string
		Client *kratosclient.OryKratos
	}
	Hydra struct {
		URL    string
		Client *hydraclient.OryHydra
	}
	JWKSURLs []string `mapstructure:"jwks_urls"`
	JWKS     *jwk.Set
	Jaeger   jaegercfg.Configuration
	Sentry   sentry.Config
}

func (cfg *Config) loadClients() error {
	hydraURL, err := url.Parse(cfg.Hydra.URL)
	if err != nil {
		return err
	}

	hydraTransport := hydraclient.TransportConfig{
		Schemes:  []string{hydraURL.Scheme},
		Host:     hydraURL.Host,
		BasePath: hydraURL.Path,
	}
	cfg.Hydra.Client = hydraclient.NewHTTPClientWithConfig(nil, &hydraTransport)

	kratosURL, err := url.Parse(cfg.Kratos.URL)
	if err != nil {
		return err
	}
	kratosTransport := kratosclient.TransportConfig{
		Schemes:  []string{kratosURL.Scheme},
		Host:     kratosURL.Host,
		BasePath: kratosURL.Path,
	}
	cfg.Kratos.Client = kratosclient.NewHTTPClientWithConfig(nil, &kratosTransport)
	return nil
}

func (cfg *Config) loadKeys() error {
	cfg.JWKS = &jwk.Set{}
	for _, u := range cfg.JWKSURLs {
		set, err := jwk.Fetch(u)
		if err != nil {
			return err
		}

		cfg.JWKS.Keys = append(cfg.JWKS.Keys, set.Keys...)
	}
	return nil
}

func init() {
	pflag.Int("http.port", 1234, "TODO")
	pflag.String("http.host", "", "TODO")
	pflag.String("kratos.url", "", "TODO")
	pflag.String("hydra.url", "", "TODO")
	pflag.StringSlice("jwks_urls", []string{}, "JWKS URLs")
	pflag.String("jaeger.servicename", "sso-ui", "Name of the service")
	pflag.String("jaeger.sampler.type", jaeger.SamplerTypeConst, "Jaeger sampler type")
	pflag.Float64("jaeger.sampler.param", 1, "Jaeger sampler param")
	pflag.Bool("jaeger.reporter.logspans", true, "Should Jaeger log spans")
	pflag.String("jaeger.reporter.collectorendpoint", "", "Jaeger Collector endpoint")
	pflag.String("sentry.dsn", "", "Sentry DSN where to send events")
	pflag.String("sentry.environment", "development", "Name of the current environment (production, test, development)")
}

func Load() *Config {
	var cfg Config
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	viper.Unmarshal(&cfg)
	if err := cfg.loadClients(); err != nil {
		log.Fatalf("config: could not load clients: %v", err)
	}

	if err := cfg.loadKeys(); err != nil {
		log.Fatalf("config: could not load keys: %v", err)
	}
	return &cfg
}
