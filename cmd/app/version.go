package main

import (
	"fmt"
)

var (
	GitCommit = "unknown"
	GitState  = ""
)

func GitVersion() string {
	if len(GitState) > 0 {
		return fmt.Sprintf("%s-%s", GitCommit, GitState)
	}
	return GitCommit
}
