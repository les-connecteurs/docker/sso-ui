package main

import (
	"fmt"
	"log"
	"net/http"

	"connecteu.rs/sso-ui/config"
	"connecteu.rs/sso-ui/handlers"
	"connecteu.rs/sso-ui/i18n"
	"connecteu.rs/sso-ui/sentry"
	"connecteu.rs/sso-ui/templates"
	"connecteu.rs/sso-ui/tracing"
)

func main() {
	c := config.Load()
	closer := tracing.Setup(c.Jaeger)
	defer closer.Close()

	sentry.Setup(c.Sentry, GitVersion())
	defer sentry.Flush()

	// Wrapping clients
	c.Kratos.Client.SetTransport(tracing.Wrap(sentry.Wrap(c.Kratos.Client.Transport, "kratos")))
	c.Hydra.Client.SetTransport(tracing.Wrap(sentry.Wrap(c.Hydra.Client.Transport, "hydra")))

	t, err := templates.Compile()
	if err != nil {
		log.Fatalf("could not load templates: %v", err)
	}

	i := i18n.NewHelper()
	t.I18nHelper = i

	log.Printf("starting version %s", GitVersion())
	http.ListenAndServe(
		fmt.Sprintf("%s:%d", c.HTTP.Host, c.HTTP.Port),
		handlers.Root(c, t),
	)
}
