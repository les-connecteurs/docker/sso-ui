module connecteu.rs/sso-ui

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535 // indirect
	github.com/getsentry/sentry-go v0.6.1
	github.com/go-openapi/loads v0.19.5 // indirect
	github.com/go-openapi/runtime v0.19.15
	github.com/go-openapi/spec v0.19.8 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/lestrrat-go/jwx v1.0.2
	github.com/nicksnyder/go-i18n/v2 v2.0.3
	github.com/opentracing-contrib/go-stdlib v1.0.0
	github.com/opentracing/opentracing-go v1.1.1-0.20190913142402-a7454ce5950e
	github.com/ory/go-acc v0.2.3 // indirect
	github.com/ory/hydra-client-go v1.4.10
	github.com/ory/jsonschema/v3 v3.0.1
	github.com/ory/kratos-client-go v0.3.0-alpha.1
	github.com/ory/x v0.0.129 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.5.1
	github.com/uber/jaeger-client-go v2.23.1+incompatible
	go.mongodb.org/mongo-driver v1.3.4 // indirect
	go.uber.org/atomic v1.6.0 // indirect
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	golang.org/x/sys v0.0.0-20200610111108-226ff32320da // indirect
	golang.org/x/text v0.3.2
	golang.org/x/tools v0.0.0-20200611032120-1fdcbd130028 // indirect
	google.golang.org/grpc v1.29.1 // indirect
)
