#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

GIT_COMMIT=${GIT_COMMIT:-$(git rev-parse --short HEAD)}
GIT_STATE=""
if [ ! -z "$(git status --porcelain)" ]; then
    GIT_STATE="dirty"
fi

cat <<EOF
STABLE_DOCKER_REPOSITORY ${DOCKER_REPOSITORY:-sso-ui}
STABLE_DOCKER_TAG ${DOCKER_TAG:-latest}
STABLE_GIT_COMMIT ${GIT_COMMIT}
STABLE_GIT_STATE ${GIT_STATE}
EOF
