#!/bin/sh

set -eu

export DOCKER_REPOSITORY="$CI_REGISTRY_IMAGE"
export DOCKER_TAG="latest"
