// +build !embed_files

// When built with bazel, those files are embed in the binary. When the
// embed_files build tag is missing, it just defines an empty map to avoid
// compilation errors.
package assets

var files map[string][]byte
