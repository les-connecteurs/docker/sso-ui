TAILWIND_CONFIG = """
	{
		theme: {
			fontFamily: {
				display: ['"Roboto Slab"', "serif"],
				body: [
					'"Open Sans"',
					'"Source Sans Pro"',
					"-apple-system",
					"BlinkMacSystemFont",
					'"Segoe UI"',
					"Roboto",
					"Oxygen-Sans",
					"Ubuntu",
					"Cantarell",
					'"Helvetica Neue"',
					"sans-serif",
				],
			},

			extend: {
				maxWidth: {
					card: '25rem',
				},
				colors: {
					"dark-green": "#1A535C",
					red: "#FF6B6B",
					teal: "#4ECDC4",
					yellow: "#FFE66D",
					dark: "#0A1F22",
					light: "#F7FFF7",
				},
			},
		},
	}
"""

PURGECSS_CONFIG = """
	{
		content: bazel.data,

		defaultExtractor: content => {
			const broadMatches = content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || []
			const innerMatches = content.match(/[^<>"'`\s.()]*[^<>"'`\s.():]/g) || []
			return broadMatches.concat(innerMatches)
		  }
	}
"""

BROWSERLIST = "> 1%"
