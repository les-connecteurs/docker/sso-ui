package assets

import (
	"bytes"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"log"
	"mime"
	"net/http"
	"path/filepath"
	"strings"
	"time"
)

type IAssetsHandler interface {
	Asset(string) *Asset
}

type Asset struct {
	content      []byte
	ContentType  string
	AbsolutePath string
	sha224       [sha256.Size224]byte
	sha256       [sha256.Size]byte
	sha384       [sha512.Size384]byte
	sha512       [sha512.Size]byte
}

func (a *Asset) etag() string {
	return fmt.Sprintf(`"%s"`, base64.RawURLEncoding.EncodeToString(a.sha384[:]))
}

func (a *Asset) SRI() string {
	return fmt.Sprintf("sha384-%s", base64.RawURLEncoding.EncodeToString(a.sha384[:]))
}

type AssetsHandler struct {
	files  map[string]Asset
	prefix string
}

func NewAssetsHandler(prefix string) *AssetsHandler {
	mappedFiles := make(map[string]Asset)
	mime.AddExtensionType(".map", "application/json")

	// Load the assets
	for key, content := range files {
		contentType := mime.TypeByExtension(filepath.Ext(key))
		mappedFiles[key] = Asset{
			content:      content,
			ContentType:  contentType,
			AbsolutePath: prefix + key,
			sha224:       sha256.Sum224(content),
			sha256:       sha256.Sum256(content),
			sha384:       sha512.Sum384(content),
			sha512:       sha512.Sum512(content),
		}
	}

	return &AssetsHandler{files: mappedFiles, prefix: prefix}
}

func (h *AssetsHandler) Asset(key string) *Asset {
	if asset, ok := h.files[key]; ok {
		return &asset
	} else {
		return nil
	}
}

func (h *AssetsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	key := strings.TrimPrefix(r.RequestURI, h.prefix)

	if asset := h.Asset(key); asset != nil {
		w.Header().Set("Content-Type", asset.ContentType)
		w.Header().Set("ETag", asset.etag())
		http.ServeContent(w, r, "", time.Time{}, bytes.NewReader(asset.content))
	} else {
		log.Printf("not found: %s", key)
		http.NotFound(w, r)
	}
}
