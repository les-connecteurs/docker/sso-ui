# SSO UI

This is kinda a test to combine [Kratos](https://www.ory.sh/docs/kratos/) and [Hydra](https://www.ory.sh/docs/hydra/) together.

It implements the login and register flows from Kratos and the login and consent flows from Hydra.
Kratos identity traits are automatically mapped to ID token claims via the schema associated with the identity.

It uses [Bazel](https://bazel.build/) to build binaries and docker images.
Run the `//:bin` target to run the app and the `//:image` to build and load the image.

# This is a work in progress

This is more a proof of concept than anything else.

Right now the hydra login flow is a bit hacky because there are no ways in Kratos to pass around data when initiating the login flow nor initiate the flow from the admin API.
Therefore, the Hydra login flow works by saving the login challenge in a cookie, performing the Kratos login flow and validating the Hydra login flow stored in that cookie once logged in. This involves *a lot* of redirections.

# Schema mapping

Consider this schema:

```yaml
$id: https://schemas.connecteu.rs/v1/user.json
$schema: http://json-schema.org/draft-07/schema#
title: A user (v1)
type: object
properties:
  email:
    title: E-Mail
    type: string
    format: email

    connecteu.rs/sso-ui:
      mapping:
        scopes: [email]
        claims: [email]

  username:
    title: Username
    type: string

    connecteu.rs/sso-ui:
      mapping:
        scopes: [profile]
        claims: [preferred_username]
```

When the `profile` scope is asked, the `username` traits is mapped to the `preferred_username` claim.
When the `email` scope is asked, the `email` traits is mapped to the `email` claim.

Note that `scopes` and `claims` are both arrays, so traits can be mapped in multiple scopes, on multiple claims.
