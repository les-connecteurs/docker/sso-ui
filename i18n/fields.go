package i18n

import (
	"github.com/nicksnyder/go-i18n/v2/i18n"
)

var fieldsMap = map[string]*i18n.Message{
	"identifier": &i18n.Message{
		ID:    "Identifier",
		Other: "Username or email address",
	},
	"password": &i18n.Message{
		ID:    "Password",
		Other: "Password",
	},
	"traits.email": &i18n.Message{
		ID:    "EmailTrait",
		Other: "Email",
	},
	"traits.username": &i18n.Message{
		ID:    "UsernameTrait",
		Other: "Username",
	},
	"traits.name.first": &i18n.Message{
		ID:    "FirstNameTrait",
		Other: "Given name",
	},
	"traits.name.last": &i18n.Message{
		ID:    "LastNameTrait",
		Other: "Last name",
	},
}

func (l Localizer) Field(field string) (string, error) {
	if message, ok := fieldsMap[field]; ok {
		return l.LocalizeMessage(message)
	}

	return field, nil
}
