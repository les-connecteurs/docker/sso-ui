load("@bazel_skylib//lib:shell.bzl", "shell")

def _go_i18n_impl(ctx):
    # That way we don't depend on defaults encoded in the binary but always
    # use defaults set on attributes of the rule
    args = [
        ctx.attr.operation,
        "-sourceLanguage=%s" % ctx.attr.source_language,
        "-outdir=%s" % ctx.attr.outdir,
        "-format=%s" % ctx.attr.format,
    ]
    for f in ctx.files.files:
        args.append(f.path)

    out_file = ctx.actions.declare_file(ctx.label.name + ".bash")
    substitutions = {
        "@@ARGS@@": shell.array_literal(args),
        "@@GOI18N_SHORT_PATH@@": shell.quote(ctx.executable._goi18n.short_path),
    }

    ctx.actions.expand_template(
        template = ctx.file._runner,
        output = out_file,
        substitutions = substitutions,
        is_executable = True,
    )

    runfiles = ctx.runfiles(files = ctx.files.files + [ctx.executable._goi18n])
    return [DefaultInfo(
        files = depset([out_file]),
        runfiles = runfiles,
        executable = out_file,
    )]

go_i18n = rule(
    implementation = _go_i18n_impl,
    attrs = {
        "files": attr.label_list(
            allow_files = True,
            mandatory = True,
            doc = "File to read messages from",
        ),
        "outdir": attr.string(
            mandatory = True,
            doc = "Output directory",
        ),
        "format": attr.string(
            default = "toml",
            doc = "Output format",
        ),
        "source_language": attr.string(
            default = "en",
            doc = "The language tag of the extracted messages",
        ),
        "operation": attr.string(
            default = "extract",
            values = ["extract", "merge"],
        ),
        "_goi18n": attr.label(
            default = "@com_github_nicksnyder_go_i18n_v2//goi18n:goi18n",
            cfg = "host",
            executable = True,
        ),
        "_runner": attr.label(
            default = "//i18n/tools:runner.bash.template",
            allow_single_file = True,
        ),
    },
    executable = True,
)
