package i18n

import (
	"errors"
	"log"

	"github.com/nicksnyder/go-i18n/v2/i18n"
	hydramodels "github.com/ory/hydra-client-go/models"
	kratosmodels "github.com/ory/kratos-client-go/models"
)

type ILocalizer interface {
	I18n() Localizer
}

type Localizer struct {
	*i18n.Localizer
}

func (l Localizer) I18n() Localizer {
	return l
}

func (l Localizer) Lang() (string, error) {
	_, tag, err := l.LocalizeWithTag(&i18n.LocalizeConfig{
		DefaultMessage: Next,
	})
	if err != nil {
		return "", err
	}

	return tag.String(), nil
}

func (l Localizer) Next() (string, error) {
	return l.LocalizeMessage(Next)
}

func (l Localizer) SigninMessage() (string, error) {
	return l.LocalizeMessage(SigninMessage)
}

func (l Localizer) RegisterMessage() (string, error) {
	return l.LocalizeMessage(RegisterMessage)
}

func (l Localizer) Signin() (string, error) {
	return l.LocalizeMessage(Signin)
}

func (l Localizer) Register() (string, error) {
	return l.LocalizeMessage(Register)
}

func (l Localizer) Hi(identity *kratosmodels.Identity) (string, error) {
	log.Printf("traits: %#v", identity.Traits)
	traits, ok := identity.Traits.(map[string]interface{})
	if !ok {
		return "", errors.New("could not cast traits")
	}
	iname, ok := traits["name"]
	if !ok {
		return "", errors.New("no name field in identity")
	}
	name, ok := iname.(map[string]interface{})
	if !ok {
		return "", errors.New("could not cast name trait")
	}
	ifirst, ok := name["first"]
	if !ok {
		return "", errors.New("no first name")
	}
	first, ok := ifirst.(string)
	if !ok {
		return "", errors.New("could not cast first name")
	}

	return l.Localize(&i18n.LocalizeConfig{
		DefaultMessage: Hi,
		TemplateData: map[string]string{
			"Name": first,
		},
	})
}

func (l Localizer) WantsAccess(client *hydramodels.OAuth2Client) (string, error) {
	return l.Localize(&i18n.LocalizeConfig{
		DefaultMessage: WantsAccess,
		TemplateData:   client,
	})
}

func (l Localizer) AllowAccess() (string, error) {
	return l.LocalizeMessage(AllowAccess)
}

func (l Localizer) DenyAccess() (string, error) {
	return l.LocalizeMessage(DenyAccess)
}
