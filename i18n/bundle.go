package i18n

import (
	"net/http"

	"github.com/BurntSushi/toml"
	"github.com/nicksnyder/go-i18n/v2/i18n"

	"golang.org/x/text/language"
)

type I18nHelper struct {
	bundle *i18n.Bundle
}

func NewHelper() *I18nHelper {
	b := i18n.NewBundle(language.English)
	b.RegisterUnmarshalFunc("toml", toml.Unmarshal)

	for key, content := range files {
		b.MustParseMessageFileBytes(content, key)
	}

	return &I18nHelper{
		bundle: b,
	}
}

// LocalizerFromRequest creates a new Localizer based on the current request
func (h *I18nHelper) LocalizerFromRequest(r *http.Request) Localizer {
	accept := r.Header.Get("Accept-Language")
	return Localizer{
		i18n.NewLocalizer(h.bundle, accept),
	}
}
