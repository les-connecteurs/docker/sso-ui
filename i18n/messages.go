package i18n

import (
	"github.com/nicksnyder/go-i18n/v2/i18n"
)

var (
	Next *i18n.Message = &i18n.Message{
		ID:    "Next",
		Other: "Next",
	}
	Signin *i18n.Message = &i18n.Message{
		ID:    "Signin",
		Other: "Sign in",
	}
	SigninMessage *i18n.Message = &i18n.Message{
		ID:    "SigninMessage",
		Other: "Sign in with your Tenoco account.",
	}
	Register *i18n.Message = &i18n.Message{
		ID:    "Register",
		Other: "Create an account",
	}
	RegisterMessage *i18n.Message = &i18n.Message{
		ID:    "RegisterMessage",
		Other: "Create your Tenoco account.",
	}
	Hi *i18n.Message = &i18n.Message{
		ID:    "Hi",
		Other: "Hi {{ .Name }}",
	}
	WantsAccess *i18n.Message = &i18n.Message{
		ID:    "WantsAccess",
		Other: "{{ or .ClientName .ClientID }} wants to access to your Tenoco account",
	}
	AllowAccess *i18n.Message = &i18n.Message{
		ID:    "AllowAccess",
		Other: "Allow",
	}
	DenyAccess *i18n.Message = &i18n.Message{
		ID:    "DenyAccess",
		Other: "Cancel",
	}
)
