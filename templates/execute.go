package templates

import (
	"io"
	"net/http"

	hydramodels "github.com/ory/hydra-client-go/models"
	kratosmodels "github.com/ory/kratos-client-go/models"
)

type registerData struct {
	*kratosmodels.RegistrationRequest
	ctx
}

func (t *Templates) Register(writer io.Writer, request *http.Request, payload *kratosmodels.RegistrationRequest) error {
	return t.register.Execute(writer, &registerData{
		payload,
		t.Globals.ctxFromRequest(request),
	})
}

type consentData struct {
	*hydramodels.ConsentRequest
	*kratosmodels.Identity
	ctx
}

func (t *Templates) Consent(writer io.Writer, request *http.Request, consent *hydramodels.ConsentRequest, identity *kratosmodels.Identity) error {
	return t.consent.Execute(writer, &consentData{
		consent,
		identity,
		t.Globals.ctxFromRequest(request),
	})
}

type errorData struct {
	*kratosmodels.ErrorContainer
	ctx
}

func (t *Templates) Error(writer io.Writer, request *http.Request, payload *kratosmodels.ErrorContainer) error {
	return t.err.Execute(writer, &errorData{
		payload,
		t.Globals.ctxFromRequest(request),
	})
}

type loginData struct {
	*kratosmodels.LoginRequest
	ctx
}

func (t *Templates) Login(writer io.Writer, request *http.Request, payload *kratosmodels.LoginRequest) error {
	return t.login.Execute(writer, &loginData{
		payload,
		t.Globals.ctxFromRequest(request),
	})
}

type profileData struct {
	*kratosmodels.SettingsRequest
	ctx
}

func (t *Templates) Profile(writer io.Writer, request *http.Request, payload *kratosmodels.SettingsRequest) error {
	return t.profile.Execute(writer, &profileData{
		payload,
		t.Globals.ctxFromRequest(request),
	})
}
