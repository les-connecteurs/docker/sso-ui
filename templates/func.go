package templates

import (
	"html/template"
	"sort"

	kratosmodels "github.com/ory/kratos-client-go/models"
)

var funcs template.FuncMap = template.FuncMap{
	"child":         child,
	"sortFields":    sortFields,
	"isHiddenField": isHiddenField,
}

type childData struct {
	Data interface{}
	Context
}

func child(data interface{}, context Context) childData {
	return childData{Data: data, Context: context}
}

const defaultWeight = 10

var fieldWeights = map[string]int{
	"identifier":      1,
	"traits.username": 2,
	"traits.email":    3,
	"password":        5,
}

func sortFields(fields kratosmodels.FormFields) kratosmodels.FormFields {
	sort.Slice(fields, func(i, j int) bool {
		wi := defaultWeight
		wj := defaultWeight
		if weight, exists := fieldWeights[*fields[i].Name]; exists {
			wi = weight
		}
		if weight, exists := fieldWeights[*fields[j].Name]; exists {
			wj = weight
		}

		if wi == wj {
			return i < j
		} else {
			return wi < wj
		}
	})
	return fields
}

func isHiddenField(field kratosmodels.FormField) bool {
	return *field.Type == "hidden"
}
