// +build !embed_files

// When built with bazel, those files are embed in the binary. When the
// embed_files build tag is missing, the files are read from the filesystem at
// runtime. This is the case when the app is built with a simple `go build`
// command.

package templates

import (
	"io/ioutil"
	"log"
	"path/filepath"
)

var files map[string]string

func init() {
	files = make(map[string]string)
	paths, err := filepath.Glob("templates/*.tmpl")
	if err != nil {
		log.Fatalf("templates: could not load templates: %v", err)
	}

	for _, path := range paths {
		content, err := ioutil.ReadFile(path)
		if err != nil {
			log.Fatalf("templates: could not read file: %v", err)
		}
		files[path] = string(content)
	}
}
