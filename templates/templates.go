package templates

import (
	"html/template"
	"net/http"

	"connecteu.rs/sso-ui/assets"
	"connecteu.rs/sso-ui/i18n"
)

// Templates holds the compiled templates
type Templates struct {
	login    *template.Template
	register *template.Template
	profile  *template.Template
	err      *template.Template
	consent  *template.Template
	*Globals
}

// Globals holds some global variables in templates
type Globals struct {
	BaseURI string
	*assets.AssetsHandler
	*i18n.I18nHelper
}

type Context interface {
	i18n.ILocalizer
	assets.IAssetsHandler
}

type ChildContext struct {
	Context
}

type ctx struct {
	i18n.Localizer
	*assets.AssetsHandler
}

func (g *Globals) ctxFromRequest(r *http.Request) ctx {
	return ctx{
		g.I18nHelper.LocalizerFromRequest(r),
		g.AssetsHandler,
	}
}

func parse(filenames ...string) (*template.Template, error) {
	var t *template.Template
	var err error

	for _, filename := range filenames {
		var tmpl *template.Template
		if t == nil {
			t = template.New(filename).Funcs(funcs)
			tmpl = t
		} else {
			tmpl = t.New(filename)
		}

		_, err = tmpl.Parse(files["templates/"+filename])
		if err != nil {
			return nil, err
		}
	}

	return t, nil
}

// Compile loads the templates
func Compile() (*Templates, error) {
	var err error
	templates := Templates{
		Globals: &Globals{
			AssetsHandler: nil,
			I18nHelper:    nil,
		},
	}

	templates.login, err = parse("login.tmpl", "form.tmpl", "layout.tmpl")
	if err != nil {
		return nil, err
	}

	templates.register, err = parse("register.tmpl", "form.tmpl", "layout.tmpl")
	if err != nil {
		return nil, err
	}

	templates.profile, err = parse("profile.tmpl", "form.tmpl", "layout.tmpl")
	if err != nil {
		return nil, err
	}

	templates.err, err = parse("error.tmpl", "form.tmpl", "layout.tmpl")
	if err != nil {
		return nil, err
	}

	templates.consent, err = parse("consent.tmpl", "layout.tmpl")
	if err != nil {
		return nil, err
	}

	return &templates, nil
}
