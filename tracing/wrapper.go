package tracing

import (
	"context"
	"net/http"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/client"
	"github.com/opentracing-contrib/go-stdlib/nethttp"
	opentracing "github.com/opentracing/opentracing-go"
)

type rt struct {
	*client.Runtime
}
type tr struct {
	transport        http.RoundTripper
	tracingTransport http.RoundTripper
}

func Wrap(t runtime.ClientTransport) runtime.ClientTransport {
	r := t.(*client.Runtime)
	r.Transport = &tr{
		transport:        r.Transport,
		tracingTransport: &nethttp.Transport{RoundTripper: r.Transport},
	}
	wrapped := &rt{Runtime: r}
	return wrapped
}

func (t *tr) RoundTrip(req *http.Request) (*http.Response, error) {
	op, ok := req.Context().Value("operation").(*runtime.ClientOperation)
	if !ok {
		return t.transport.RoundTrip(req)
	}

	req, ht := nethttp.TraceRequest(
		opentracing.GlobalTracer(),
		req,
		nethttp.OperationName(op.ID),
	)
	defer ht.Finish()

	return t.tracingTransport.RoundTrip(req)
}

func (r *rt) Submit(operation *runtime.ClientOperation) (interface{}, error) {
	pctx := operation.Context
	if pctx == nil {
		pctx = r.Runtime.Context
	}
	if pctx == nil {
		pctx = context.Background()
	}

	ctx := context.WithValue(pctx, "operation", operation)
	operation.Context = ctx

	return r.Runtime.Submit(operation)
}
