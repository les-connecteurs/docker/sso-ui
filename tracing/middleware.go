package tracing

import (
	"net/http"

	"github.com/opentracing-contrib/go-stdlib/nethttp"
	opentracing "github.com/opentracing/opentracing-go"
)

func Middleware(handler http.Handler) http.Handler {
	return nethttp.Middleware(opentracing.GlobalTracer(), handler)
}
