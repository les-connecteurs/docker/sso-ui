package tracing

import (
	"io"
	"log"

	opentracing "github.com/opentracing/opentracing-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
)

func Setup(config jaegercfg.Configuration) io.Closer {
	tracer, closer, err := config.NewTracer()
	if err != nil {
		log.Fatalf("main: could not initialize jaeger: %v", err)
	}
	opentracing.SetGlobalTracer(tracer)

	return closer
}
