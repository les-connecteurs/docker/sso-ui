package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	hydraadmin "github.com/ory/hydra-client-go/client/admin"
	hydramodels "github.com/ory/hydra-client-go/models"
	"github.com/ory/jsonschema/v3"
	kratosadmin "github.com/ory/kratos-client-go/client/admin"
	kratoscommon "github.com/ory/kratos-client-go/client/common"

	"connecteu.rs/sso-ui/config"
	"connecteu.rs/sso-ui/schema"
	"connecteu.rs/sso-ui/templates"
)

type ConsentHandler struct {
	*config.Config
	*templates.Templates
}

func NewConsentHandler(c *config.Config, t *templates.Templates) http.Handler {
	return &ConsentHandler{
		Config:    c,
		Templates: t,
	}
}

func (h *ConsentHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	u, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not parse request: %v", err))
		return
	}

	challenge := u.Query().Get("consent_challenge")
	if len(challenge) == 0 {
		ErrorHandler(w, r, fmt.Errorf("no consent_challenge"))
		return
	}

	// Fetch the consent request
	p := hydraadmin.NewGetConsentRequestParams().
		WithContext(ctx).
		WithConsentChallenge(challenge)
	resp, err := h.Config.Hydra.Client.Admin.GetConsentRequest(p)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not get consent request: %v", err))
		return
	}

	// Ask Kratos for the identity
	p2 := kratosadmin.NewGetIdentityParams().
		WithContext(ctx).
		WithID(resp.Payload.Subject)
	idResp, err := h.Config.Kratos.Client.Admin.GetIdentity(p2)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not get identity: %v", err))
		return
	}

	// Load the schema for this identity
	// TODO: cache this somehow
	p3 := kratoscommon.NewGetSchemaParams().
		WithContext(ctx).
		WithID(*idResp.Payload.TraitsSchemaID)
	schemaResp, err := h.Config.Kratos.Client.Common.GetSchema(p3)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("failed to load schema from kratos: %v", err))
		return
	}

	rawSchema, err := json.Marshal(schemaResp.Payload)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("failed to marshal schema: %v", err))
		return
	}

	// Initialize the mapper
	mapper, err := schema.NewMapperExtension()
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not initiate mapper extension: %v", err))
		return
	}

	// and parse the schema
	compiler := jsonschema.NewCompiler()
	mapper.Register(compiler)
	if err := compiler.AddResource(idResp.Payload.TraitsSchemaURL, bytes.NewBuffer(rawSchema)); err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not load schema: %v", err))
		return
	}

	traitsSchema, err := compiler.Compile(idResp.Payload.TraitsSchemaURL)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not compile schema: %v", err))
		return
	}

	// Validate the user's traits to load up its claims in the mapper
	if err := traitsSchema.ValidateInterface(idResp.Payload.Traits); err != nil {
		ErrorHandler(w, r, fmt.Errorf("could validate traits against schema: %v", err))
		return
	}

	// PostForm was submitted, let's accept/deny the request
	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			ErrorHandler(w, r, fmt.Errorf("could not parse form: %v", err))
		}
		if r.PostForm.Get("submit") == "deny" {
			body := hydramodels.RejectRequest{
				Error:            "access_denied",
				ErrorDescription: "The resource owner denied the request",
			}
			p := hydraadmin.NewRejectConsentRequestParams().
				WithContext(ctx).
				WithConsentChallenge(challenge).
				WithBody(&body)
			resp, err := h.Config.Hydra.Client.Admin.RejectConsentRequest(p)
			if err != nil {
				ErrorHandler(w, r, fmt.Errorf("could not reject consent request: %v", err))
				return
			}

			http.Redirect(w, r, resp.Payload.RedirectTo, http.StatusTemporaryRedirect)
			return
		}

		remember, err := strconv.ParseBool(r.PostForm.Get("remember"))
		if err != nil {
			remember = false
		}
		scopes := r.PostForm["grant_scope"]
		// Derive the claims from the mapper
		claims := mapper.DeriveSession(scopes)
		body := hydramodels.AcceptConsentRequest{
			GrantScope:               scopes,
			GrantAccessTokenAudience: resp.Payload.RequestedAccessTokenAudience,
			Session: &hydramodels.ConsentRequestSession{
				IDToken: claims,
			},

			Remember:    remember,
			RememberFor: 3600,
		}
		p := hydraadmin.NewAcceptConsentRequestParams().
			WithContext(ctx).
			WithConsentChallenge(challenge).
			WithBody(&body)
		resp, err := h.Config.Hydra.Client.Admin.AcceptConsentRequest(p)
		if err != nil {
			ErrorHandler(w, r, fmt.Errorf("could not accept consent request: %v", err))
			return
		}
		http.Redirect(w, r, resp.Payload.RedirectTo, http.StatusTemporaryRedirect)
		return
	}

	// Hydra asked us to skip the consent, blindly accept it & populate the session
	if resp.Payload.Skip {
		scopes := resp.Payload.RequestedScope
		// Derive the claims from the mapper
		claims := mapper.DeriveSession(scopes)
		body := hydramodels.AcceptConsentRequest{
			GrantScope:               scopes,
			GrantAccessTokenAudience: resp.Payload.RequestedAccessTokenAudience,
			Session: &hydramodels.ConsentRequestSession{
				IDToken: claims,
			},
		}
		p := hydraadmin.NewAcceptConsentRequestParams().
			WithContext(ctx).
			WithConsentChallenge(challenge).
			WithBody(&body)
		resp, err := h.Config.Hydra.Client.Admin.AcceptConsentRequest(p)
		if err != nil {
			ErrorHandler(w, r, fmt.Errorf("could not accept consent request: %v", err))
			return
		}
		http.Redirect(w, r, resp.Payload.RedirectTo, http.StatusTemporaryRedirect)
		return
	}

	if err := h.Templates.Consent(w, r, resp.Payload, idResp.Payload); err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not execute template: %v", err))
		return
	}
}
