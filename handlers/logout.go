package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/lestrrat-go/jwx/jws"
	"github.com/lestrrat-go/jwx/jwt"
	hydraadmin "github.com/ory/hydra-client-go/client/admin"

	"connecteu.rs/sso-ui/config"
)

type LogoutHandler struct {
	*config.Config
}

func NewLogoutHandler(c *config.Config) http.Handler {
	return &LogoutHandler{
		Config: c,
	}
}

func (h *LogoutHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// The token should be set by oathkeeper
	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer")
	if len(splitToken) != 2 {
		ErrorHandler(w, r, fmt.Errorf("invalid token: %v", reqToken))
		return
	}
	rawToken := strings.TrimSpace(splitToken[1])
	payload, err := jws.VerifyWithJWKSet([]byte(rawToken), h.Config.JWKS, nil)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not validate token: %v", err))
		return
	}
	var token = jwt.New()
	if err := json.Unmarshal(payload, token); err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not parse jwt: %v", err))
		return
	}

	if token.Subject() != "" {
		// Logging out of kratos
		http.Redirect(w, r, "/self-service/browser/flows/logout", http.StatusTemporaryRedirect)
		return
	}

	challenge, err := ChallengeFromRequest(r, "logout")
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not fetch challenge cookie: %v", err))
		return
	}

	// Resetting the challenge cookie
	c := ChallengeCookie(nil, r.URL)
	c.MaxAge = -1
	http.SetCookie(w, c)

	p := hydraadmin.NewAcceptLogoutRequestParams().
		WithContext(r.Context()).
		WithLogoutChallenge(challenge)
	resp, err := h.Config.Hydra.Client.Admin.AcceptLogoutRequest(p)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("failed to accept logout request: %v", err))
		return
	}
	http.Redirect(w, r, resp.Payload.RedirectTo, http.StatusTemporaryRedirect)
}
