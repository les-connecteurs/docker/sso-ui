package handlers

import (
	"fmt"
	"net/http"
	"net/url"

	kratoscommon "github.com/ory/kratos-client-go/client/common"

	"connecteu.rs/sso-ui/config"
	"connecteu.rs/sso-ui/templates"
)

type UserErrorHandler struct {
	*config.Config
	*templates.Templates
}

func NewUserErrorHandler(c *config.Config, t *templates.Templates) http.Handler {
	return &UserErrorHandler{
		Config:    c,
		Templates: t,
	}
}

func (h *UserErrorHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	u, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not parse request: %v", err))
		return
	}

	errorID := u.Query().Get("error")
	if len(errorID) == 0 {
		ErrorHandler(w, r, fmt.Errorf("no error"))
		return
	}

	p := kratoscommon.NewGetSelfServiceErrorParams().
		WithContext(r.Context()).
		WithError(&errorID)
	resp, err := h.Config.Kratos.Client.Common.GetSelfServiceError(p)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not get error: %v", err))
		return
	}

	err = h.Templates.Error(w, r, resp.Payload)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not execute template: %v", err))
		return
	}
}
