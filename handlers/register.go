package handlers

import (
	"fmt"
	"net/http"
	"net/url"

	kratoscommon "github.com/ory/kratos-client-go/client/common"

	"connecteu.rs/sso-ui/config"
	"connecteu.rs/sso-ui/templates"
)

type RegisterHandler struct {
	*config.Config
	*templates.Templates
}

func NewRegisterHandler(c *config.Config, t *templates.Templates) http.Handler {
	return &RegisterHandler{
		Config:    c,
		Templates: t,
	}
}

func (h *RegisterHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	u, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not parse request: %v", err))
		return
	}

	reqID := u.Query().Get("request")
	if len(reqID) == 0 {
		// TODO: derive from kratos public URL
		http.Redirect(w, r, "/self-service/browser/flows/registration", http.StatusTemporaryRedirect)
		return
	}

	p := kratoscommon.NewGetSelfServiceBrowserRegistrationRequestParams().
		WithContext(r.Context()).
		WithRequest(reqID)
	resp, err := h.Config.Kratos.Client.Common.GetSelfServiceBrowserRegistrationRequest(p)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not get register request: %v", err))
		return
	}

	err = h.Templates.Register(w, r, resp.Payload)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not execute template: %v", err))
		return
	}
}
