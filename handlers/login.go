package handlers

import (
	"fmt"
	"net/http"
	"net/url"

	kratoscommon "github.com/ory/kratos-client-go/client/common"

	"connecteu.rs/sso-ui/config"
	"connecteu.rs/sso-ui/templates"
)

type LoginHandler struct {
	*config.Config
	*templates.Templates
}

func NewLoginHandler(c *config.Config, t *templates.Templates) http.Handler {
	return &LoginHandler{
		Config:    c,
		Templates: t,
	}
}

func (h *LoginHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	u, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not parse request: %v", err))
		return
	}

	reqID := u.Query().Get("request")
	if len(reqID) == 0 {
		// TODO: derive from kratos public URL
		http.Redirect(w, r, "/self-service/browser/flows/login", http.StatusTemporaryRedirect)
		return
	}

	p := kratoscommon.NewGetSelfServiceBrowserLoginRequestParams().
		WithContext(r.Context()).
		WithRequest(reqID)
	resp, err := h.Config.Kratos.Client.Common.GetSelfServiceBrowserLoginRequest(p)
	if err != nil {
		// TODO: error handling
		ErrorHandler(w, r, fmt.Errorf("could not get login request: %v", err))
		return
	}

	err = h.Templates.Login(w, r, resp.Payload)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not execute template: %v", err))
		return
	}
}
