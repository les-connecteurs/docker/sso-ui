package handlers

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

type ChallengeHandler struct {
	challengeType string
	redirectURL   string
}

func NewChallengeHandler(challengeType, redirectURL string) http.Handler {
	return &ChallengeHandler{
		challengeType,
		redirectURL,
	}
}

type challengeCookie struct {
	Type      string `json:"type"`
	Challenge string `json:"challenge"`
}

const (
	ChallengeCookieName = "sso_ui_challenge"
)

func ChallengeCookie(challenge *challengeCookie, url *url.URL) *http.Cookie {
	var challengeStr string
	if challenge != nil {
		raw, err := json.Marshal(challenge)
		if err != nil {
			log.Fatalf("could not marshal challenge cookie: %v", err)
		}

		challengeStr = base64.RawStdEncoding.EncodeToString(raw)
	}

	return &http.Cookie{
		Name:     ChallengeCookieName,
		Value:    challengeStr,
		Domain:   url.Hostname(),
		SameSite: http.SameSiteLaxMode,
		Path:     "/ui",
		HttpOnly: true,
		Secure:   (url.Scheme == "https"),
		MaxAge:   3600,
	}
}

func ChallengeFromRequest(r *http.Request, challengeType string) (string, error) {
	c, err := r.Cookie(ChallengeCookieName)
	if err != nil {
		return "", fmt.Errorf("no challenge cookie")
	}

	rawChallengeCookie, err := base64.RawStdEncoding.DecodeString(c.Value)
	if err != nil {
		return "", fmt.Errorf("could not decode challenge cookie: %v", err)
	}

	var challenge challengeCookie
	if err := json.Unmarshal(rawChallengeCookie, &challenge); err != nil {
		return "", fmt.Errorf("could not parse challenge cookie: %v", err)
	}

	if challenge.Type == challengeType {
		return challenge.Challenge, nil
	}

	return "", fmt.Errorf("invalid challenge type: expected %s, got %s", challengeType, challenge.Type)

}

func (h *ChallengeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	u, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not parse request: %v", err))
		return
	}

	challenge := u.Query().Get(h.challengeType + "_challenge")
	if len(challenge) == 0 {
		ErrorHandler(w, r, fmt.Errorf("no %s_challenge", h.challengeType))
		return
	}

	cookie := ChallengeCookie(&challengeCookie{Type: h.challengeType, Challenge: challenge}, r.URL)
	http.SetCookie(w, cookie)
	http.Redirect(w, r, h.redirectURL, http.StatusTemporaryRedirect)
}
