package handlers

import (
	"net/http"
	"os"

	"github.com/getsentry/sentry-go/http"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	"connecteu.rs/sso-ui/assets"
	"connecteu.rs/sso-ui/config"
	"connecteu.rs/sso-ui/templates"
	"connecteu.rs/sso-ui/tracing"
)

func Root(config *config.Config, templates *templates.Templates) http.Handler {
	assetsHandler := assets.NewAssetsHandler("/ui/assets/")
	templates.AssetsHandler = assetsHandler

	r := mux.NewRouter()
	r.Handle("/ui/login/challenge", NewChallengeHandler("login", "/ui/login/validate"))
	r.Handle("/ui/login/validate", NewValidateHandler(config))
	r.Handle("/ui/login", NewLoginHandler(config, templates))
	r.Handle("/ui/logout/challenge", NewChallengeHandler("logout", "/ui/logout"))
	r.Handle("/ui/logout", NewLogoutHandler(config))
	r.Handle("/ui/consent", NewConsentHandler(config, templates))
	r.Handle("/ui/register", NewRegisterHandler(config, templates))
	r.Handle("/ui/profile", NewProfileHandler(config, templates))
	r.Handle("/ui/error", NewUserErrorHandler(config, templates))
	r.PathPrefix("/ui/assets").Handler(assetsHandler)

	// Wrap the handler with a few middlewares
	var handler http.Handler
	handler = r
	handler = handlers.LoggingHandler(os.Stdout, handler)
	handler = tracing.Middleware(handler)
	handler = sentryhttp.New(sentryhttp.Options{}).Handle(handler)
	handler = handlers.ProxyHeaders(handler)
	return handler
}
