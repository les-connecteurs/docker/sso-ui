package handlers

import (
	"log"
	"net/http"

	"github.com/getsentry/sentry-go"
)

// ErrorHandler writes the error back to the browser and logs it
func ErrorHandler(w http.ResponseWriter, r *http.Request, err error) {
	hub := sentry.GetHubFromContext(r.Context())
	hub.RecoverWithContext(r.Context(), err)

	log.Println(err.Error())
	w.Header().Add("Content-Type", "text/plain")
	w.WriteHeader(500)
	w.Write([]byte(err.Error()))
}
