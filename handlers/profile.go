package handlers

import (
	"fmt"
	"net/http"
	"net/url"

	kratoscommon "github.com/ory/kratos-client-go/client/common"

	"connecteu.rs/sso-ui/config"
	"connecteu.rs/sso-ui/templates"
)

type ProfileHandler struct {
	*config.Config
	*templates.Templates
}

func NewProfileHandler(c *config.Config, t *templates.Templates) http.Handler {
	return &ProfileHandler{
		Config:    c,
		Templates: t,
	}
}

func (h *ProfileHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	u, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not parse request: %v", err))
		return
	}

	reqID := u.Query().Get("request")
	if len(reqID) == 0 {
		// TODO: derive from kratos public URL
		http.Redirect(w, r, "/self-service/browser/flows/settings", http.StatusTemporaryRedirect)
		return
	}

	p := kratoscommon.NewGetSelfServiceBrowserSettingsRequestParams().
		WithContext(r.Context()).
		WithRequest(reqID)
	resp, err := h.Config.Kratos.Client.Common.GetSelfServiceBrowserSettingsRequest(p)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not get profile request: %v", err))
		return
	}

	err = h.Templates.Profile(w, r, resp.Payload)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not execute template: %v", err))
		return
	}
}
