package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/getsentry/sentry-go"
	"github.com/lestrrat-go/jwx/jws"
	"github.com/lestrrat-go/jwx/jwt"
	hydraadmin "github.com/ory/hydra-client-go/client/admin"
	hydramodels "github.com/ory/hydra-client-go/models"

	"connecteu.rs/sso-ui/config"
)

type ValidateHandler struct {
	*config.Config
}

func NewValidateHandler(c *config.Config) http.Handler {
	return &ValidateHandler{
		Config: c,
	}
}

func (h *ValidateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	hub := sentry.GetHubFromContext(ctx)

	// The token should be set by oathkeeper
	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer")
	if len(splitToken) != 2 {
		ErrorHandler(w, r, fmt.Errorf("invalid token: %v", reqToken))
		return
	}
	rawToken := strings.TrimSpace(splitToken[1])
	payload, err := jws.VerifyWithJWKSet([]byte(rawToken), h.Config.JWKS, nil)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not validate token: %v", err))
		return
	}
	token := jwt.New()
	if err := json.Unmarshal(payload, token); err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not parse jwt: %v", err))
		return
	}

	hub.ConfigureScope(func(scope *sentry.Scope) {
		scope.SetUser(sentry.User{
			ID: token.Subject(),
		})
	})

	challenge, err := ChallengeFromRequest(r, "login")
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("could not fetch challenge cookie: %v", err))
		return
	}

	// TODO: login accept UI
	s := token.Subject()
	body := &hydramodels.AcceptLoginRequest{
		Subject:     &s,
		Remember:    true,
		RememberFor: 60 * 60 * 7,
	}

	// Resetting the challenge cookie
	c := ChallengeCookie(nil, r.URL)
	c.MaxAge = -1
	http.SetCookie(w, c)

	p := hydraadmin.NewAcceptLoginRequestParams().
		WithContext(ctx).
		WithBody(body).
		WithLoginChallenge(challenge)
	resp, err := h.Config.Hydra.Client.Admin.AcceptLoginRequest(p)
	if err != nil {
		ErrorHandler(w, r, fmt.Errorf("failed to accept login request: %v", err))
		return
	}
	http.Redirect(w, r, resp.Payload.RedirectTo, http.StatusTemporaryRedirect)
}
