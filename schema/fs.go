// +build !embed_files

package schema

import (
	"io/ioutil"
	"log"
)

var schema string

const schemaPath = "schema/extension.schema.json"

func init() {
	content, err := ioutil.ReadFile(schemaPath)
	if err != nil {
		log.Fatalf("schema: could not load '%s': %v", schemaPath, err)
	}
	schema = string(content)
}
