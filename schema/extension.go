package schema

import (
	"bytes"
	"encoding/json"
	"fmt"

	"github.com/ory/jsonschema/v3"
)

const (
	extensionName = "connecteu.rs/sso-ui"
)

type Mapping struct {
	Scopes []string `json:"scopes"`
	Claims []string `json:"claims"`
}

type extensionConfig struct {
	*Mapping `json:"mapping"`
}

type MapperExtension struct {
	requestedScopes []string
	mappings        []Mapping
	schema          *jsonschema.Schema
	mappedClaims    map[string]map[string]interface{}
}

// NewMapperExtension creates a new mapper instance
func NewMapperExtension() (*MapperExtension, error) {
	var err error
	e := new(MapperExtension)

	e.schema, err = jsonschema.CompileString(extensionName, schema)
	if err != nil {
		return nil, err
	}
	e.mappedClaims = make(map[string]map[string]interface{})

	return e, nil
}

// Register the extension to a jsonschema.Compiler
func (e *MapperExtension) Register(compiler *jsonschema.Compiler) *MapperExtension {
	compiler.Extensions[extensionName] = e.Extension()
	return e
}

// Extension returns the jsonschema extension object for the mapper
func (e *MapperExtension) Extension() jsonschema.Extension {
	return jsonschema.Extension{
		Meta:     e.schema,
		Compile:  e.compile,
		Validate: e.validate,
	}
}

func (e *MapperExtension) compile(ctx jsonschema.CompilerContext, m map[string]interface{}) (interface{}, error) {
	if raw, ok := m[extensionName]; ok {
		var b bytes.Buffer
		if err := json.NewEncoder(&b).Encode(raw); err != nil {
			return nil, err
		}

		var e extensionConfig
		if err := json.NewDecoder(&b).Decode(&e); err != nil {
			return nil, err
		}

		return &e, nil
	}
	return nil, nil
}

func (e *MapperExtension) validate(ctx jsonschema.ValidationContext, s interface{}, v interface{}) error {
	c, ok := s.(*extensionConfig)
	if !ok {
		return nil
	}

	for _, scope := range c.Scopes {
		claims, ok := e.mappedClaims[scope]
		if !ok {
			claims = make(map[string]interface{})
		}

		for _, claim := range c.Claims {
			if _, ok := claims[claim]; ok {
				return fmt.Errorf("duplicate claim '%s' in scope '%s'", claim, scope)
			}
			claims[claim] = v
		}

		e.mappedClaims[scope] = claims
	}

	return nil
}

// DeriveSession returns claims mapped from the traits for the given scope list
func (e *MapperExtension) DeriveSession(scopes []string) map[string]interface{} {
	session := make(map[string]interface{})
	for _, scope := range scopes {
		if claims, ok := e.mappedClaims[scope]; ok {
			for claim, value := range claims {
				session[claim] = value
			}
		}
	}
	return session
}
