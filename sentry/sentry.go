package sentry

import (
	"log"
	"net/http"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/client"
)

type tr struct {
	transport http.RoundTripper
	category  string
}

type Config struct {
	DSN         string
	Environment string
}

func Flush() {
	sentry.Flush(2 * time.Second)
}

func Setup(cfg Config, version string) {
	err := sentry.Init(sentry.ClientOptions{
		Dsn:         cfg.DSN,
		Environment: cfg.Environment,
		Release:     version,
		Debug:       true,
	})

	if err != nil {
		log.Fatalf("could not init sentry: %v", err)
	}
}

func Wrap(t runtime.ClientTransport, category string) runtime.ClientTransport {
	r := t.(*client.Runtime)
	r.Transport = &tr{
		transport: r.Transport,
		category:  category,
	}
	return r
}

func (t *tr) RoundTrip(req *http.Request) (*http.Response, error) {
	ctx := req.Context()
	hub := sentry.GetHubFromContext(ctx)
	bc := sentry.Breadcrumb{
		Category: t.category,
		Type:     "http",
		Data: map[string]interface{}{
			"url":    req.URL.String(),
			"method": req.Method,
		},
	}

	resp, err := t.transport.RoundTrip(req)

	if resp != nil {
		bc.Data["status_code"] = resp.StatusCode
		bc.Data["reason"] = resp.Status
	}

	hub.AddBreadcrumb(&bc, &sentry.BreadcrumbHint{})
	return resp, err
}
